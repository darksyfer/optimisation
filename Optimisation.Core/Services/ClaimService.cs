﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Optimisation.Core.Entities;
using Optimisation.Core.Utilities;

namespace Optimisation.Core.Services
{
    public class ClaimService
    {
        private string _blockText;
        private string _costCentre;
        private decimal _GST;
        private decimal _costBeforeGST;
        private decimal _total;
        ExpenseClaim _expnseClaim = new ExpenseClaim();
        public ClaimService(string blocktext)
        {
            _blockText = blocktext;
        }

        public bool AreAllTagsClosed()
        {
            var tags = Helper.GetUnclosedOrUnopenedTagsFromText(_blockText); // returns unaccounted opening and/or closing tags
            return tags.Any() ? false : true;
        }

        public bool HasTotal()
        {
            var present = Helper.IsTotalPresent(_blockText);
            return present;
        }

        public void CalculateGST()
        {
            GetTotalValue();
            IsCostCentreMissing();
            _GST = (_total / 150) * 15;
            _costBeforeGST = _total - _GST;
            _expnseClaim = new ExpenseClaim
            {
                GST = decimal.Round(_GST, 2, MidpointRounding.AwayFromZero),
                TotalWithoutGST = decimal.Round(_costBeforeGST, 2, MidpointRounding.AwayFromZero)
            };

            //return _expnseClaim;

        }

        private void GetTotalValue()
        {
            _total = Helper.GetTotal(_blockText);
            
        }

        private void IsCostCentreMissing()
        {
            _costCentre = Helper.GetCostCentre(_blockText);

            //return present;
        }

        //public bool IsBlockTextValid
        //{
        //    get
        //    {
        //        return _blockValid;
        //    }
        //}

        public string GetCostCentre
        {
            get
            {
                return _costCentre;
            }
        }
        public decimal GetTotal
        {
            get
            {
                return _total;
            }
        }

        public ExpenseClaim GetExpenseClaim
        {
            get
            {
                return _expnseClaim;
            }
        }
    }
}
