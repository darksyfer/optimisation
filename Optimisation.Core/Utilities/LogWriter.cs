﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Optimisation.Core.Utilities
{
    public class LogWriter
    {
        public LogWriter(string logMessage, string blocktext)
        {
            LogWrite(logMessage, blocktext);
        }
        private void LogWrite(string logMessage, string blocktext)
        {
            try
            {
                DateTime currentDate = DateTime.Now;
                string fileName = currentDate.Year + "-" + currentDate.Month + "-" + currentDate.Day + "_log.txt";
                using (StreamWriter w = System.IO.File.AppendText(fileName))
                {
                    LogWrite(logMessage, blocktext, w);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void LogWrite(string logMessage, string blocktext, StreamWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("{0} :{1} - {2}", DateTime.Now, logMessage, blocktext);
                //txtWriter.Write(" :{0}", logMessage);
            }
            catch (Exception ex)
            {

            }

        }




    }
}
