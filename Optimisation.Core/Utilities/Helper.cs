﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimisation.Core.Utilities
{
    public class Helper
    {
        /// <summary>
        /// Fucntion that checks the validity of the text being submitted
        /// </summary>
        /// <param name="blocktext"></param>
        /// <returns>List of Tags</returns>
        public static List<string> GetUnclosedOrUnopenedTagsFromText(string blocktext)
        {
            List<string> openingTags = new List<string>();
            int noOfChar = blocktext.Length;
            for (int i = 0; i < blocktext.Length - 1; i++)
            {
                var openTag = "";
                var currentChar = blocktext[i];
                if (currentChar == '<')
                {
                    while (currentChar != '>' && i < (noOfChar - 1))
                    {
                        openTag = string.Concat(openTag, currentChar);
                        i++;
                        currentChar = blocktext[i];
                    }
                    openTag = string.Concat(openTag, currentChar);
                    if (!string.IsNullOrEmpty(openTag))
                    {
                        var tag = GetTrimmedText(openTag);
                        var tagKey = "";
                        if (tag[1] == '/' && tag.Contains('>'))
                        {
                            int indexOfCloseTag = 2;
                            int indexOfCloseBracket = tag.LastIndexOf(">");
                            tagKey = tag.Substring(indexOfCloseTag, indexOfCloseBracket - 2);
                            openingTags.RemoveAll(tag => tag.Contains(tagKey));
                        }
                        else
                        {
                            openingTags.Add(tag);
                        }
                    }
                }
            }
            return openingTags;
        }

        private static string GetTrimmedText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                return String.Concat(text.Where(c => !Char.IsWhiteSpace(c)));
            }
            throw new NotImplementedException();
        }

        public static bool IsTotalPresent(string blocktext)
        {
            if (GetTrimmedText(blocktext).Contains("<total>"))
            {
                return true;
                //return blocktext.IndexOf("<total>");
            }
            return false;
        }

        public static Decimal GetTotal(string blocktext)
        {
            var trimmedBlockText = GetTrimmedText(blocktext);
            if (trimmedBlockText.Contains("<total>"))
            {
                int pointerFrom = trimmedBlockText.IndexOf("<total>") + "<total>".Length;
                int pointerTo = trimmedBlockText.IndexOf("</total>");
                Decimal total = Decimal.Parse(trimmedBlockText.Substring(pointerFrom, pointerTo - pointerFrom));
                return total;
                //return blocktext.IndexOf("<total>");
            }
            return 0;
        }

        public static string GetCostCentre(string blocktext)
        {
            var trimmedBlockText = GetTrimmedText(blocktext);
            if (trimmedBlockText.Contains("<cost_centre>"))
            {
                int pointerFrom = trimmedBlockText.IndexOf("<cost_centre>") + "<cost_centre>".Length;
                int pointerTo = trimmedBlockText.IndexOf("</cost_centre");
                string cost_centre = trimmedBlockText.Substring(pointerFrom, pointerTo - pointerFrom);
                return cost_centre;
                //return blocktext.IndexOf("<total>");
            }
            return "UNKNOWN";
        }
    }
}
