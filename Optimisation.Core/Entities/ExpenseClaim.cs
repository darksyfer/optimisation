﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimisation.Core.Entities
{
    public class ExpenseClaim
    {
        public decimal GST { get; set; }
        public decimal TotalWithoutGST { get; set; }
    }
}
