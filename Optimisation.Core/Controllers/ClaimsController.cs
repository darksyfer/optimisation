﻿using Microsoft.AspNetCore.Mvc;
using Optimisation.Core.Services;
using Optimisation.Core.Utilities;
using System;
using System.Threading.Tasks;

namespace Optimisation.Core.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [RequireHttps]
    [ApiController]    
    public class ClaimsController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get([FromBody]string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return Ok("Claim API v1.0");
            }
            try
            {
                ClaimService _TotalService = new ClaimService(content);
                if (!_TotalService.AreAllTagsClosed())
                {
                    var message = string.Format("The blocktext does not have corresponding opening and closing tags", content);
                    return BadRequest(message);
                }

                if (!_TotalService.HasTotal())
                {
                    return BadRequest("No total");
                }

                _TotalService.CalculateGST();
                return Ok(_TotalService.GetExpenseClaim);
            }
            catch (Exception e)
            {
                LogWriter logwriter = new LogWriter(e.Message, content);
                return BadRequest("An error has occured");
            }
            

            //_TotalService.IsCostCentreMissing();
            //return Ok(_TotalService.GetCostCentre);

            
        }
    }
}
