using NUnit.Framework;
using Optimisation.Core.Services;
using Optimisation.Core.Utilities;

namespace Optimisation.Test
{
    [TestFixture]
    public class BlockTextTest
    {
        private Helper _helper;       

        [SetUp]
        public void Setup()
        {
            _helper = new Helper();
        }

        [Test]
        public void Test_for_HasAllClosingAndOpeningTag()
        {
            string blocktext = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense > ";
            ClaimService _claimService = new ClaimService(blocktext);
            var result = _claimService.AreAllTagsClosed();
            Assert.IsTrue(result);
        }

        [Test]
        public void Test_for_DoesNotHasAllClosingAndOpeningTag()
        {
            string blocktext = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense  ";
            ClaimService _claimService = new ClaimService(blocktext);
            var result = _claimService.AreAllTagsClosed();
            Assert.IsFalse(result);
        }

        [Test]
        public void Test_for_HasTotal()
        {
            string blocktext = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense  >";
            ClaimService _claimService = new ClaimService(blocktext);
            var result = _claimService.HasTotal();
            Assert.IsTrue(result);
        }


        [Test]
        public void Test_for_DoesHasTotal()
        {
            string blocktext = "<expense>< cost_centre > DEV002 </ cost_centre >< nottotal > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense  >";
            ClaimService _claimService = new ClaimService(blocktext);
            var result = _claimService.HasTotal();
            Assert.IsFalse(result);
        }

        [Test]
        public void Test_for_CostCentre()
        {
            string blocktext = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense  >";
            ClaimService _claimService = new ClaimService(blocktext);
            _claimService.CalculateGST();
           
            var result = _claimService.GetCostCentre;
            Assert.AreEqual(result, "DEV002");
        }

        [Test]
        public void Test_for_CostCentreNotPresent()
        {
            string blocktext = "<expense>< not_cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense  >";
            ClaimService _claimService = new ClaimService(blocktext);
            _claimService.CalculateGST();

            var result = _claimService.GetCostCentre;
            Assert.AreEqual(result, "UNKNOWN");
        }

        //[Test]
        //public void IsTextPresent()
        //{
        //    var text = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense > ";
        //    var result = _textvalidity.IsTextEmptyOrNull(text);

        //    Assert.AreEqual(result, false);
        //}

        //[Test]
        //public void HasExpenseTag()
        //{
        //    var text = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense >";
        //    var result = _textvalidity.AreAllTagsClosed(text);

        //    Assert.AreEqual(result, "<expense>");
        //}

        //[Test]
        //public void HasAllOpeningTagsBeenClosed()
        //{
        //    var text = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense >";
        //    var result = _textvalidity.AreAllTagsClosed(text).Count;

        //    Assert.AreEqual(result, 0);
        //}

        //[Test]
        //public void HasTotalTagInBlockText()
        //{
        //    var text = "<expense>< cost_centre > DEV002 </ cost_centre >< total > 1024.01 </ total >< payment_method > personal card </ payment_method >   </ expense >";
        //    var result = _textvalidity.TextHasTotal(text);

        //    Assert.IsTrue(result);
        //}
    }
}